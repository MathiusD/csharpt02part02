﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tableau
{
    public class Program
    {
        public static int TablSearch(int[] input, int search)
        {
            int retour = 0, indice = 0, taille=input.Length;
            bool find = false;
            while (find == false)
            {
                if (input[indice] == search)
                {
                    retour = indice + 1;
                    find = true;
                }
                indice++;
                if (indice >= taille)
                {
                    find = true;
                }
            }
            return retour;
        }
        static void Main(string[] args)
        {
            int[] input = new int[10];
            int test, output;
            Console.WriteLine("Saisissez dix valeurs :");
            for (int indice = 0; indice < 10; indice++)
            {
                Console.WriteLine("{0}{1}", (indice + 1), " :");
                int.TryParse(Console.ReadLine(), out input[indice]);
            }
            Console.WriteLine("Saisissez une valeur :");
            int.TryParse(Console.ReadLine(), out test);
            output = TablSearch(input, test);
            if (output==0)
            {
                Console.WriteLine("La valeur en question n'a pas été saisie");
            } else
            {
                Console.WriteLine("{0}{1}{2}{3}", test, " est la ", output, "-eme valeur saisie.");
            }
            Console.WriteLine("Appuyez sur une touche pour mettre fin au programme.");
            Console.ReadKey();
        }
    }
}
