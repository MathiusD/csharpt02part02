﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Tableau;

namespace TableauTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TablSearchTest()
        {
            Assert.AreEqual(5, Program.TablSearch(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }, 5));
            Assert.AreEqual(0, Program.TablSearch(new int[] { 1, 2, 3, 4, 45, 6, 7, 8, 9, 10 }, 5));
        }
    }
}
